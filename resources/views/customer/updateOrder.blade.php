@extends('layouts.app')
@section('title', 'Customer')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Update Order</h1>
                </div>
                <div class="col-6 d-flex flex-row-reverse">

                </div>
            </div>

            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        
                        <form method="POST" action="{{ route('customer-order.update', $id) }}" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Product Category</label>
                                        <select class="form-control " name="product_category">
                                            <option value="Frozen Products" <?php if ($order->product_category == "Frozen Products") {
                                                echo 'selected';
                                            } ?> >Frozen Products</option>
                                            <option value="Foods & Drinking Products" <?php if ($order->product_category == "Foods & Drinking Products") {
                                                echo 'selected';
                                            } ?> >Foods & Drinking Products</option>
                                            <option value="Furniture, Electronic Goods" <?php if ($order->product_category == "Furniture, Electronic Goods") {
                                                echo 'selected';
                                            } ?> >Furniture, Electronic Goods</option>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Weight</label><code>(in KG)</code>
                                        <input type="number" class="form-control" name="product_weight" value="{{ $order->product_weight }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Delivered To</label>
                                        <input type="text" class="form-control" name="delivered_to" value="{{ $order->delivered_to }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Pickup Adress</label>
                                        <textarea type="text" class="form-control" placeholder="Enter Full Address"
                                            name="pickup_address" value="">{{ $order->pickup_address }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Destination Adress</label>
                                        <textarea type="text" class="form-control" placeholder="Enter Full Address"
                                            name="destination_address" value="">{{ $order->destination_address }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-phone"></i>
                                                </div>
                                            </div>
                                            <input type="number" class="form-control phone-number" name="contact_number" value="{{ $order->contact_number }}">
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" id="id" value="{{ $order->id }}"
                                    class="form-control form-control-lg" />

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('extra-js')


    <script>

    </script>
@endsection
