@extends('layouts.app')
@section('title', 'Customer')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Running Orders</h1>
                </div>

            </div>

            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Order Date</th>
                                            <th>Delivered To</th>
                                            <th>Contact Number</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($runningOrderList as $key => $order)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</td>
                                                <td>{{ $order->delivered_to }}</td>
                                                <td>{{ $order->contact_number }}</td>
                                                <td>
                                                    @if ($order->order_status == -1)
                                                        Reject
                                                    @elseif($order->order_status == 0)
                                                        Pending
                                                    @elseif($order->order_status == 1)
                                                        Accepted
                                                    @elseif($order->order_status == 2)
                                                        Received
                                                    @elseif($order->order_status == 3)
                                                        On the way
                                                    @elseif($order->order_status == 5)
                                                        Delivered
                                                    @endif
                                                </td>
                                                {{-- <td>
                                                <div class="badge badge-warning">Unpaid</div>
                                            </td> --}}
                                                <td>
                                                    <a href="{{ route('order.order-details-customer', $order->id) }}"
                                                        class="btn btn-primary"> View Details </a>

                                                    <a type="button" href="{{ route('customer-order.edit', $order->id) }}"
                                                        class="btn btn-warning">
                                                        Edit
                                                    </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>


@endsection

@section('extra-js')

@endsection
