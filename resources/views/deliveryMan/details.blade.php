@extends('layouts.app')
@section('title', 'Delivery Man')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Profile</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="#">Dashboard</a>
                </div>
                <div class="breadcrumb-item">Profile</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">{{ $deliveryMan->name }}</h2>
            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-5">
                    <div class="card profile-widget">
                        <div class="profile-widget-header">

                            <img alt="image" @if ($deliveryMan->photo == 'default.jpg') src="{{ asset('../admin/img/avatar/avatar-1.png') }}"
                            @else src="{{ asset('images/deliveryMan/' . $deliveryMan->photo) }}" @endif class="rounded-circle profile-widget-picture" />
                            <div class="profile-widget-items">
                                <div class="profile-widget-item">
                                    <div class="profile-widget-item-label">
                                        Completed Delivery
                                    </div>
                                    <div class="profile-widget-item-value"> {{ $complete_order }}</div>
                                </div>
                                <div class="profile-widget-item">
                                    <div class="profile-widget-item-label">
                                        Running Delivery
                                    </div>
                                    <div class="profile-widget-item-value"> {{ $running_order }}</div>
                                </div>
                                <div class="profile-widget-item">
                                    <div class="profile-widget-item-label">
                                        Cancelled Delivery
                                    </div>
                                    <div class="profile-widget-item-value"> {{ $cancel_order }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-widget-description">
                            <ul class="list-group">
                                <li class="
                      list-group-item
                      d-flex
                      justify-content-between
                      align-items-center
                    ">
                                    Contact Number
                                    <span class="badge badge-primary badge-pill">{{ $deliveryMan->phone_no }}</span>
                                </li>
                                <li class="
                      list-group-item
                      d-flex
                      justify-content-between
                      align-items-center
                    ">
                                    Email
                                    <span class="badge badge-primary badge-pill">{{ $deliveryMan->email }}</span>
                                </li>
                                <li class="
                      list-group-item
                      d-flex
                      justify-content-between
                      align-items-center
                    ">
                                    Status
                                    <span class="badge badge-primary badge-pill">
                                        @if ($deliveryMan->status == 1) Active
                                        @else Inactive
                                        @endif
                                    </span>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-7">
                    <div class="card">
                        <div class="card-header">
                            <h4>History</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Track Number</th>
                                            <th>Order Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($runningOrderList as $key => $order)
                                        <?php
                                        $status = "";
                                        if ($order->order_status == -1) {
                                            $status = "Reject";
                                        } else if ($order->order_status == 0) {
                                            $status = "Pending";
                                        } else if ($order->order_status == 1) {
                                            $status = "Accept";
                                        } else if ($order->order_status == 2) {
                                            $status = "Receive";
                                        } else if ($order->order_status == 3) {
                                            $status = "On the way";
                                        } else if ($order->order_status == 5) {
                                            $status = "Delivered";
                                        }

                                        ?>
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>#354354</td>
                                            <td> {{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</td>
                                            <td> {{ $status }} </td>
                                            <td>
                                                <a href="{{ route('order.order-details-admin', $order->id) }}" class="btn btn-primary"> View Details </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('extra-js')


<script>
    function changeModalData(man) {
        console.log(man);

        deliveryManData = JSON.parse(man);

        document.getElementById("id").value = deliveryManData.id;
        document.getElementById("name").value = deliveryManData.name;
        document.getElementById("phone_no").value = deliveryManData.phone_no;
        document.getElementById("email").value = deliveryManData.email;
        document.getElementById("address").value = deliveryManData.address;


    }
</script>
@endsection