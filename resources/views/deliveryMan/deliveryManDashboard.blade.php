@extends('layouts.app')

@section('content')
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-12">
        @if ($errors->any())
        <div class="col-sm-12">
          <div class="alert  alert-warning alert-dismissible fade show" role="alert">
            @foreach ($errors->all() as $error)
            <span>
              <p>{{ $error }}</p>
            </span>
            @endforeach
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        @endif

        @include('flash-message')
        <div class="card card-statistic-2">
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-archive"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Completed</h4>
            </div>
            <div class="card-body">
              {{ $totalCompletedOrders }}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-dollar-sign"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Running</h4>
            </div>
            <div class="card-body">
              {{ $totalRunningOrders }}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-shopping-bag"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>New Orders</h4>
            </div>
            <div class="card-body">
              {{ $totalNewOrders }}
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4>Running Orders</h4>
            <div class="card-header-action">
              <a href="{{ route('order.assign-order-list') }}" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
            </div>
          </div>
          <div class="card-body p-0">
            <div class="table-responsive table-invoice">
              <table class="table table-striped">
                <thead>
                  <tr>
                      <th>#</th>
                      <th>Customer</th>
                      <th>Product Category</th>
                      <th>Weight</th>
                      <th>Status</th>
                      <th>Order Date</th>
                      <th>Delivered To</th>
                      <th>Contact Number</th>
                      <th>Details</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($runningOrderList as $key => $order)

                  <?php
                  $status = "";
                  if ($order->order_status == -1) {
                      $status = "Reject";
                  } else if ($order->order_status == 0) {
                      $status = "Pending";
                  } else if ($order->order_status == 1) {
                      $status = "Accept";
                  } else if ($order->order_status == 2) {
                      $status = "Receive";
                  } else if ($order->order_status == 3) {
                      $status = "On the way";
                  } else if ($order->order_status == 5) {
                      $status = "Delivered";
                  }

                  ?>
                  <tr>
                      <td>{{ $key + 1 }}</td>

                      <td> <a href="{{ route('customer.detailsCustomer', $order->customer->id) }}"> {{ $order->customer->name }} </a> </td>

                      

                      <td>{{ $order->product_category }}</td>

                      <td>{{ $order->product_weight }}</td>
                      
                     

                      <td>{{ $status}}</td>


                      <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</td>
                      <td>{{ $order->delivered_to }}</td>
                      <td>{{ $order->contact_number }}</td>
                      <td>
                      <a href="{{ route('order.order-details', $order->id) }}"  class="btn btn-primary">  <i class="far fa-eye"></i> </a>
                      </td>
                  </tr>


                  @endforeach
              </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection