@extends('layouts.app')
@section('title', 'Delivery Man')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Delivery Man List</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>

        @if ($errors->any())
            <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @include('flash-message')
        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Details</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deliveryMans as $key => $man)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $man->name }}</td>
                                            <td>{{ $man->phone_no }}</td>
                                            <td>{{ $man->email }}</td>

                                            <td>
                                                <a class="btn btn-info"
                                                    href="{{ route('delivery-man.details', $man->id) }}">
                                                    View
                                                </a>
                                            </td>
                                            <td>

                                                <a class="btn btn-primary"
                                                    onclick="changeModalData( '{{ $man }}')"
                                                    data-toggle="modal" data-target="#modal-part2">
                                                    Edit
                                                </a>
                                                <form method="POST"
                                                    action="{{ route('delivery-man.delete', $man->id) }}"
                                                    style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class=" btn btn-danger"
                                                        onclick="return confirm('Confirm delete?')"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                    </button>
                                                </form>
                                            </td>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            <form method="POST" action="{{ route('delivery-man.add') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title">Delivery Man Add</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <code>*</code></label>
                            <input type="text" name="name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Phone Number<code>*</code></label>
                            <input type="number" name="phone_no" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Email <code>*</code></label>
                            <input type="email" name="email" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Address<code>*</code></label>
                            <textarea type="text" name="address" class="form-control form-control-lg"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Photo <code></code></label>
                            <input type="file" name="photo" class="form-control form-control-lg" />
                        </div>


                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <!-- End -->

    <!-- Edit -->
    <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <form method="POST" action="{{ route('delivery-man.updateData') }}" enctype="multipart/form-data">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Delivery Man Profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Name <code>*</code></label>
                            <input type="text" name="name" id="name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Phone Number<code>*</code></label>
                            <input type="number" name="phone_no" id="phone_no" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Email <code>*</code></label>
                            <input type="email" name="email" id="email" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Address<code>*</code></label>
                            <textarea type="text" name="address" class="form-control form-control-lg"
                                id="address"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Photo <code></code></label>
                            <input type="file" name="photo" class="form-control form-control-lg" />
                        </div>
                        <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                    </div>

                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="editProductHead" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('extra-js')


<script>
    function changeModalData(man) {
        console.log(man);

        deliveryManData = JSON.parse(man);

        document.getElementById("id").value = deliveryManData.id;
        document.getElementById("name").value = deliveryManData.name;
        document.getElementById("phone_no").value = deliveryManData.phone_no;
        document.getElementById("email").value = deliveryManData.email;
        document.getElementById("address").value = deliveryManData.address;


    }
</script>
@endsection
