<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            @if (Auth::user()->role_id == 1)
                <a href="{{ route('adminDashboard') }}">Delivery Tracker</a>
            @elseif(Auth::user()->role_id == 2)
                <a href="{{ route('deliveryManDashboard') }}">Delivery Tracker</a>
            @else
                <a href="{{ route('customerDashboard') }}">Delivery Tracker</a>
            @endif
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">DT</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Quick Links</li>


            @if (Auth::user()->role_id == 1)
                <li>
                    <a class="nav-link" href="{{ route('admin.list') }}"><i class="fas fa-user-shield"></i>
                        <span>Admin
                            List</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('customer.list') }}"><i class="fas fa-users"></i>
                        <span>Customer
                            List</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('delivery-man.list') }}"><i class="fas fa-shipping-fast"></i>
                        <span>Delivery Man List</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('admin.getDeliveryOrderForAdmin') }}"><i
                            class="fas fa-hourglass-start"></i>
                        <span>Delivery Requests</span></a>
                </li>

                <li>
                    <a class="nav-link" href="{{ route('admin.adminOrderHistory') }}"><i
                            class="fas fa-hourglass-start"></i>
                        <span>Order History</span></a>
                </li>
            @endif

            @if (Auth::user()->role_id == 2)
                <li>
                    <a class="nav-link" href="{{ route('order.assign-order-list') }}"><i
                            class="fas fa-hourglass-start"></i>
                        <span>Assign Order List</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('order.delivery-man-completed') }}"><i
                            class="fas fa-history"></i> <span>Completed Orders</span></a>
                </li>
            @endif


            @if (Auth::user()->role_id == 3)
                <li>
                    <a class="nav-link" href="{{ route('customer-order.newOrderRequest') }}"><i
                            class="fas fa-cart-plus"></i> <span>New Order</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('customer-order.runnngOrder') }}"><i
                            class="fas fa-running"></i> <span>Running Order</span></a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('customer-order.orderHistory') }}"><i
                            class="fas fa-tasks"></i>
                        <span>Orders List</span></a>
                </li>

            @endif
        </ul>
        </li>

    </aside>
</div>
