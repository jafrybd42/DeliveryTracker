@extends('layouts.app')
@section('title', 'Customer')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Assigned Order <code>(Action)</code></h1>
                </div>
                <div class="col-6 d-flex flex-row-reverse">
                    <!-- <button
                      class="btn btn-primary"
                      data-toggle="modal"
                      data-target="#modal-part"
                    >
                      + Add New
                    </button> -->
                </div>
            </div>

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="row card-body p-0">
                            <div class="col-lg-4">
                                <ul class="list-group">
                                    <h4>Details</h1>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Customer Name
                                            <span
                                                class="badge badge-primary badge-pill">{{ $order->customer->name }}</span>
                                        </li>
                                        @if ($order->deliveryMan_id != null)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                Delivery Man
                                                <span
                                                    class="badge badge-primary badge-pill">{{ $order->deliveryMan->name }}</span>
                                            </li>
                                        @endif

                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Order Date
                                            <span
                                                class="badge badge-primary badge-pill">{{ \Carbon\Carbon::parse($order->created_at)->format('d F, Y') }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Product Category
                                            <span class="badge badge-primary badge-pill">
                                                {{ $order->product_category }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Weight
                                            <span
                                                class="badge badge-primary badge-pill">{{ $order->product_weight }}</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            Cost
                                            <span class="badge badge-primary badge-pill">BDT 750 (TK)</span>
                                        </li>
                                        <div class="mt-5 col-12 text-right" id="show-element">
                                            <p id="demo"></p>
                                            <!-- <button class="btn btn-primary" onclick="getLocation()">
                                                Recieve
                                            </button> -->

                                            @if ($order->order_status == 1 && $user_role_id == 2)
                                                <form method="POST" action="{{ route('order.assign-order-received') }}">
                                                    @csrf
                                                    <input type="hidden" name="receive_id" id="receive_id"
                                                        value="{{ $order->id }}">
                                                    <input type="hidden" name="receive_lat" id="receive_lat" value="0">
                                                    <input type="hidden" name="receive_lag" id="receive_lag" value="0">
                                                    <input type="submit" value="Receive Product" class="btn btn-primary">
                                                </form>
                                            @endif
                                        </div>
                                </ul>
                            </div>
                            <div class="col-lg-8 hide" id="to-show">
                                <h4>Status</h4>

                                @if ($user_role_id == 1 or $user_role_id == 3)
                                    <div class="col-12 row text-right pb-3">
                                        <a href="{{ route('order.map', $id) }}">
                                            <button class="btn btn-primary">
                                                See in Map
                                            </button>
                                        </a>

                                    </div>

                                @else


                                    <div class="col-12 row text-right pb-3">

                                        @if ($order->order_status == 2 or $order->order_status == 3 or $order->order_status == 5)

                                            @if (count($orderActionStatus) < 5 and $order->order_status != 5)
                                                <form method="POST"
                                                    action="{{ route('order.assign-order-add-location') }}">
                                                    @csrf
                                                    <input type="hidden" name="add_location_id" id="add_location_id"
                                                        value="{{ $order->id }}">
                                                    <input type="hidden" name="add_location_lat" id="add_location_lat"
                                                        value="0">
                                                    <input type="hidden" name="add_location_lag" id="add_location_lag"
                                                        value="0">
                                                    <input type="submit" value="Add Location" class="btn btn-primary mr-2">
                                                </form>

                                            @endif


                                            @if ($order->order_status != 5)
                                                <form method="POST" action="{{ route('order.assign-order-complete') }}">
                                                    @csrf
                                                    <input type="hidden" name="order_complete_id" id="order_complete_id"
                                                        value="{{ $order->id }}">
                                                    <input type="hidden" name="order_complete_lat" id="order_complete_lat"
                                                        value="0">
                                                    <input type="hidden" name="order_complete_lag" id="order_complete_lag"
                                                        value="0">
                                                    <input type="submit" value=" Complete Order"
                                                        class="btn btn-primary mr-2">
                                                </form>
                                            @endif

                                            <a href="{{ route('order.map', $id) }}">
                                                <button class="btn btn-primary">
                                                    See in Map
                                                </button>
                                            </a>



                                        @endif

                                    </div>



                                @endif

                                <div class="table-responsive" id="target">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>Track Location</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            @foreach ($orderActionStatus as $key => $status)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td> {{ \Carbon\Carbon::parse($status->created_at)->format('d F, Y') }}
                                                    </td>
                                                    <td>{{ \Carbon\Carbon::parse($status->created_at)->format('h:i a') }}
                                                    </td>
                                                    <td><span class="badge badge-primary badge-pill">
                                                            {{ $status->details }}</span></td>
                                                    <td> {{ $status->track_location }}</td>
                                                    <td> {{ $status->latitude }}</td>
                                                    <td> {{ $status->longitude }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>



@endsection

@section('extra-js')

    <script>
        var x = document.getElementById("demo");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(setPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function setPosition(position) {
            // x.innerHTML = "Latitude: " + position.coords.latitude +
            //     "<br>Longitude: " + position.coords.longitude;

            let lat = position.coords.latitude;
            let lng = position.coords.longitude;

            try {
                document.getElementById("receive_lat").value = lat;
                document.getElementById("receive_lag").value = lng;
            } catch (error) {

            }

            try {
                document.getElementById("add_location_lat").value = lat;
                document.getElementById("add_location_lag").value = lng;
            } catch (error) {

            }

            try {
                document.getElementById("order_complete_lat").value = lat;
                document.getElementById("order_complete_lag").value = lng;
            } catch (error) {
                console.log("error");
            }
        }

        window.onload = getLocation();
    </script>

@endsection
