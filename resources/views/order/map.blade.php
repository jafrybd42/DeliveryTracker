@extends('layouts.app')
@section('title', 'Customer')

@section('extra-css')
{{-- <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4i6uIZr3fBfWhNbsP1XKjaOQfiVd0Qso&callback=initMap" async></script> --}}
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4i6uIZr3fBfWhNbsP1XKjaOQfiVd0Qso" ></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwhT1WskklyEiMICYzsX9R1O0ZNN-_4Us&callback=initialize" async defer></script>  -->
@endsection


@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Map</h1>
            </div>

        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4i6uIZr3fBfWhNbsP1XKjaOQfiVd0Qso&callback=initMap&libraries=places" type="text/javascript"></script> -->
                        <div id="map-canvas"></div>
                        <div id="route"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>

</div>


<input type="hidden" name="orderActionStatus" id="orderActionStatus" class="form-control form-control-lg" readonly value="{{ $orderActionStatus }}" />



@endsection

@section('extra-js')


<script>
    function initMap() {



        var orderActionStatus = document.getElementById("orderActionStatus").value;
        orderActionStatus = JSON.parse(orderActionStatus);

        console.log(orderActionStatus);

        var points = [];
        var markers = [];
        var label = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

        for (let i = 0; i < orderActionStatus.length; i++) {
            points.push(new google.maps.LatLng(orderActionStatus[i].latitude, orderActionStatus[i].longitude));
        }

        if (points.length > 1) {

            myOptions = {
                    zoom: 11,
                    center: points[0]
                },

                console.log(points);

            map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
                // Instantiate a directions service.
                directionsService = new google.maps.DirectionsService,
                directionsDisplay = new google.maps.DirectionsRenderer({
                    map: map
                });

            for (let i = 0; i < orderActionStatus.length; i++) {
                points.push(new google.maps.LatLng(orderActionStatus[i].latitude, orderActionStatus[i].longitude));
                markers.push(
                    new google.maps.Marker({
                        position: points[i],
                        title: orderActionStatus[i].details,
                        label: label[i],
                        map: map
                    })
                )
            }
            // get route from A to B
            // calculateAndDisplayRoute(directionsService, directionsDisplay, points[0], points[1]);
        }

    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
        directionsService.route({
            origin: pointA,
            destination: pointB,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                pointsArray = response.routes[0].legs[0].steps;
                route = "";
                for (var i in pointsArray) {
                    route += "<li>" + pointsArray[i].instructions + "</li>";
                }
                document.getElementById("route").innerHTML = "<ul>" + route + "</ul>";
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    initMap();
</script>
@endsection